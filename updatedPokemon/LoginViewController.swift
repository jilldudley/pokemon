//
//  LoginViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/6/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var textFieldLoginEmail: UITextField!
    @IBOutlet weak var textFieldLoginPassword: UITextField!
    
    //let loginSuccess = "loginSuccess"
    
    override func viewDidLoad() {
      super.viewDidLoad()
      let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
      view.addGestureRecognizer(tap)
      
    //  Auth.auth().addStateDidChangeListener() { auth, user in
     //   if user != nil {
         // self.performSegue(withIdentifier: self.loginToList, sender: nil)
      //    self.textFieldLoginEmail.text = nil
      //    self.textFieldLoginPassword.text = nil
      //  }
     // }
    }
    
    @IBAction func loginDidTouch(_ sender: Any) {
      guard
        let email = textFieldLoginEmail.text,
        let password = textFieldLoginPassword.text,
        email.count > 0,
        password.count > 0
        else {
          return
      }
      
      Auth.auth().signIn(withEmail: email, password: password) { user, error in
        if let error = error, user == nil {
          let alert = UIAlertController(title: "Sign In Failed",
                                        message: error.localizedDescription,
                                        preferredStyle: .alert)
          
          alert.addAction(UIAlertAction(title: "OK", style: .default))
          
          self.present(alert, animated: true, completion: nil)
        }
        self.performSegue(withIdentifier: "loginSuccess", sender: nil)
      }
    }
    
    

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc func dismissKeyboard(){
      view.endEditing(true)
    }
}
