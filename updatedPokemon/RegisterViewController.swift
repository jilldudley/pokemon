//
//  RegisterViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/6/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth

class RegisterViewController: UIViewController {

    @IBOutlet weak var textFieldLoginEmail: UITextField!
    @IBOutlet weak var textFieldLoginPassword: UITextField!
    
    //let loginToList = "LoginToList"
    
    override func viewDidLoad() {
      super.viewDidLoad()
      let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
      view.addGestureRecognizer(tap)
      //Auth.auth().addStateDidChangeListener() { auth, user in
       // if user != nil {
          //self.performSegue(withIdentifier: self.loginToList, sender: nil)
        //  self.textFieldLoginEmail.text = nil
        //  self.textFieldLoginPassword.text = nil
        //}
      //}
    }
    
    @IBAction func registerUser(_ sender: Any){
        /*
      let alert = UIAlertController(title: "Register",
                                    message: "Register",
                                    preferredStyle: .alert)
      
      let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
        
        let emailField = alert.textFields![0]
        let passwordField = alert.textFields![1]
        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { user, error in
          if error == nil {
            Auth.auth().signIn(withEmail: self.textFieldLoginEmail.text!,
                               password: self.textFieldLoginPassword.text!)
          }
        }
      }
      
      let cancelAction = UIAlertAction(title: "Cancel",
                                       style: .cancel)
      
      alert.addTextField { textEmail in
        textEmail.placeholder = "Enter your email"
      }
      
      alert.addTextField { textPassword in
        textPassword.isSecureTextEntry = true
        textPassword.placeholder = "Enter your password"
      }
      
      alert.addAction(saveAction)
      alert.addAction(cancelAction)
      
      present(alert, animated: true, completion: nil)
 */
        guard
          let email = textFieldLoginEmail.text,
          let password = textFieldLoginPassword.text,
          email.count > 0,
          password.count > 0
          else {
            return
        }
        Auth.auth().createUser(withEmail: self.textFieldLoginEmail.text!, password: self.textFieldLoginEmail.text!) { user, error in
          if error == nil {
            Auth.auth().signIn(withEmail: self.textFieldLoginEmail.text!,
                               password: self.textFieldLoginPassword.text!)
          }
        }
        self.performSegue(withIdentifier: "registerSuccess", sender: nil)
    }
    
    

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc func dismissKeyboard(){
      view.endEditing(true)
    }
    
}

