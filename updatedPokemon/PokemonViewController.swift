//
//  FindBattleViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/11/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth


class PokemonViewController: UIViewController {
    
	@IBOutlet weak var p1:UIButton?
    @IBOutlet weak var p2:UIButton?
    @IBOutlet weak var p3:UIButton?
    @IBOutlet weak var coachmark: UILabel!

	
    var user: User!
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    let ref = Database.database().reference(withPath: "online-users")
	var pokemon:String = "pikachu"
	var attack:Double = 13.0
	var defense:Double = 7.0
	var health:Int  = 50
	var speed:Double = 10.0
	var moves = ["thunder", "strike", "nightmare", "shock"]
	var wins = 0
	
	@IBAction func p1press()
	{
		guard let ptype = p1?.titleLabel?.text else {
		  return
		}
		self.pokemon = ptype.lowercased()
		self.attack = 6 + Double(wins) * 0.1
		self.defense = 11 + Double(wins) * 0.2
		self.health = 55 + wins * 1
		self.speed = 12 + Double(wins) * 0.2
		moves = ["water", "drip", "punch", "strike"]
		coachmark.text = "Pokemon: " + self.pokemon
		coachmark.text! += "\nAttack: " + String(self.attack)
		coachmark.text! += "\nDefense: " + String(self.defense)
		coachmark.text! += "\nSpeed: " + String(self.speed)
		coachmark.text! += "\n Health: " + String(self.health)
	}
	
	@IBAction func p2press()
	{
		guard let ptype = p2?.titleLabel?.text else {
		  return
		}
		self.pokemon = ptype.lowercased()
		self.attack = 13 + Double(wins) * 0.2
		self.defense = 7 + Double(wins) * 0.1
		self.health = 50 + wins * 1
		self.speed = 10 + Double(wins) * 0.2
		self.moves = ["thunder", "strike", "nightmare", "shock"]
		coachmark.text = "Pokemon: " + self.pokemon
		coachmark.text! += "\nAttack: " + String(self.attack)
		coachmark.text! += "\nDefense: " + String(self.defense)
		coachmark.text! += "\nSpeed: " + String(self.speed)
		coachmark.text! += "\n Health: " + String(self.health)
	}
	
	@IBAction func p3press()
	{
		guard let ptype = p3?.titleLabel?.text else {
		  return
		}
		self.pokemon = ptype.lowercased()
		self.attack = 11 + Double(wins) * 0.2
		self.defense = 9 + Double(wins) * 0.2
		self.health = 60 + wins * 1
		self.speed = 4 + Double(wins) * 0.1
		self.moves = ["fire", "spit", "fly", "punch"]
		coachmark.text = "Pokemon: " + self.pokemon
		coachmark.text! += "\nAttack: " + String(self.attack)
		coachmark.text! += "\nDefense: " + String(self.defense)
		coachmark.text! += "\nSpeed: " + String(self.speed)
		coachmark.text! += "\n Health: " + String(self.health)
	}
	

	
    override func viewDidLoad() {
      super.viewDidLoad()
		let badchar: Set<Character> = ["@", "."]
		var nocharemail = thecurremail
		nocharemail!.removeAll(where: { badchar.contains($0) })
		ref.child(nocharemail!).runTransactionBlock({ (currentData1: MutableData) -> TransactionResult in
			if let post = currentData1.value as? [String : AnyObject]{
				self.wins = post["wins"] as? Int ?? 0
				return TransactionResult.success(withValue: currentData1)
			}
			return TransactionResult.success(withValue: currentData1)
		}){ (error, committed, snapshot) in
			if let error = error {
				print(error.localizedDescription)
			}
		}
		self.attack += Double(wins) * 0.2
		self.defense += Double(wins) * 0.1
		self.speed += Double(wins) * 0.2
		self.health += wins * 1
		coachmark.numberOfLines = 5
    }

    
	override func prepare(for segue: UIStoryboardSegue, sender: Any?){
		if segue.destination is FindBattleViewController{
			let vc = segue.destination as? FindBattleViewController
			let badchar: Set<Character> = ["@", "."]
			var nocharemail = thecurremail
			nocharemail!.removeAll(where: { badchar.contains($0) })
			ref.child(nocharemail!).runTransactionBlock({ (currentData1: MutableData) -> TransactionResult in
				if let post = currentData1.value as? [String : AnyObject]{
					self.ref.child("\(nocharemail!)/char").setValue(self.pokemon)
					self.ref.child("\(nocharemail!)/attack").setValue(self.attack)
					self.ref.child("\(nocharemail!)/defense").setValue(self.defense)
					self.ref.child("\(nocharemail!)/speed").setValue(self.speed)
					self.ref.child("\(nocharemail!)/health").setValue(self.health)
					return TransactionResult.success(withValue: currentData1)
				}
				return TransactionResult.success(withValue: currentData1)
			}){ (error, committed, snapshot) in
				if let error = error {
					print(error.localizedDescription)
				}
			}
		}
	}

    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
	
	
}
