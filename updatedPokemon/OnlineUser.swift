

import Foundation
import Firebase

struct OnlineUser {
  
  let ref: DatabaseReference?
  let key: String
  let addedByUser: String
	let wins: Int
  let latval: Double
  let longval: Double
  let ingame: Bool
	let char: String
	let attack: Double
	let defense: Double
	let health: Int
	let speed: Double
  
    init(addedByUser: String, latval: Double, longval: Double, key: String = "") {
    self.ref = nil
    self.key = key
    self.addedByUser = addedByUser
    self.latval = latval
    self.longval = longval
    self.ingame = false
	self.wins = 0
		self.char = ""
		self.attack = 0
		self.defense = 0
		self.speed = 0
		self.health = 0
  }
  
  init?(snapshot: DataSnapshot) {
    guard
      let value = snapshot.value as? [String: AnyObject],
      let addedByUser = value["addedByUser"] as? String,
      let latval = value["latval"] as? Double,
      let longval = value["longval"] as? Double,
      let ingame = value["ingame"] as? Bool,
	  let wins = value["wins"] as? Int,
		let attack = value["attack"] as? Double,
		let defense = value["defense"] as? Double,
		let speed = value["speed"] as? Double,
		let health = value["health"] as? Int,
	let char = value["char"] as? String
        else {
      return nil
    }
    
    self.ref = snapshot.ref
    self.key = snapshot.key
    self.addedByUser = addedByUser
    self.latval = latval
    self.longval = longval
    self.ingame = ingame
	self.wins = wins
	self.char = char
	self.attack = attack
	self.defense = defense
	self.speed = speed
	self.health = health
  }
  
  func toAnyObject() -> Any {
    return [
      "addedByUser": addedByUser,
      "latval": latval,
      "longval": longval,
      "ingame": ingame,
	  "char": char,
	  "wins": wins
    ]
  }
}
