//
//  LeaderboardViewController.swift
//  updatedPokemon
//
//  Created by Derek Lore on 2/26/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import CoreLocation


class LeaderboardViewController: UITableViewController, CLLocationManagerDelegate {
    
    var Users: [OnlineUser] = []
    let user_ref = Database.database().reference(withPath: "online-users")
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    var userLat: Double = 0.0
    var userLong: Double = 0.0

    
    override func viewDidLoad() {
      super.viewDidLoad()
		user_ref.queryOrdered(byChild: "latval").observe(.value, with: { snapshot in
			var newUsers: [OnlineUser] = []
			for child in snapshot.children {
				if let snapshot = child as? DataSnapshot,
					let onlineUsers = OnlineUser(snapshot: snapshot) {
					if (onlineUsers.addedByUser == self.curruser?.email){
						self.userLat = onlineUsers.latval
						self.userLong = onlineUsers.longval
					}
				}
			}
			for child in snapshot.children {
			  if let snapshot = child as? DataSnapshot,
				let onlineUsers = OnlineUser(snapshot: snapshot) {
				//if (onlineUsers.addedByUser != self.curruser?.email){
					print(self.userLat, self.userLong)
					if ((abs(onlineUsers.latval - self.userLat) < 0.1) && (abs(onlineUsers.longval - self.userLong) < 0.1)){
						newUsers.append(onlineUsers)
					}
				//}
			  }
			}
			self.Users = newUsers
			print(newUsers)
			self.user_ref.removeAllObservers()
			self.Users.sort(by: {$0.wins > $1.wins})
			self.tableView.reloadData()
        })
    }

    // MARK: UITableView Delegate methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return Users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
      let onlineUser = Users[indexPath.row]
		cell.textLabel?.text = onlineUser.addedByUser + " score is: " + String(onlineUser.wins)
      cell.detailTextLabel?.text = "nearby!"
      return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
      return true
    }
    
	
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
