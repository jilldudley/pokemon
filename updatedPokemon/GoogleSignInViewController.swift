//
//  GoogleSignInViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/23/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import AVFoundation

class GoogleSignInViewController: UIViewController, GIDSignInDelegate {

//    @IBOutlet weak var textFieldLoginEmail: UITextField!
  //  @IBOutlet weak var textFieldLoginPassword: UITextField!
    
    //let loginSuccess = "loginSuccess"
    
    override func viewDidLoad() {
      super.viewDidLoad()
        MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "Music Files/Blazer Rail")
        
    GIDSignIn.sharedInstance()?.presentingViewController = self
    GIDSignIn.sharedInstance().delegate = self
 //     let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: //self, action: "dismissKeyboard")
      //view.addGestureRecognizer(tap)
      
    //  Auth.auth().addStateDidChangeListener() { auth, user in
     //   if user != nil {
         // self.performSegue(withIdentifier: self.loginToList, sender: nil)
      //    self.textFieldLoginEmail.text = nil
      //    self.textFieldLoginPassword.text = nil
      //  }
     // }
    }
    
    @IBAction func googleSignInPressed(_ sender: Any) {
         GIDSignIn.sharedInstance().signIn()
    }
    /*
    @IBAction func loginDidTouch(_ sender: Any) {
      guard
        let email = textFieldLoginEmail.text,
        let password = textFieldLoginPassword.text,
        email.count > 0,
        password.count > 0
        else {
          return
      }
        print("logging in: '" + email + "'")
        print("logging in: '" + password + "'")
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
        if let error = error, user == nil {
          let alert = UIAlertController(title: "Sign In Failed",
                                        message: error.localizedDescription,
                                        preferredStyle: .alert)
          
          alert.addAction(UIAlertAction(title: "OK", style: .default))
          
          self.present(alert, animated: true, completion: nil)
        }
        self.performSegue(withIdentifier: "loginSuccess", sender: nil)
      }
    }
    */
    

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc func dismissKeyboard(){
      view.endEditing(true)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    //Sign in functionality will be handled here
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let auth = user.authentication else { return }
        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
        Auth.auth().signIn(with: credentials) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.performSegue(withIdentifier: "loginSuccess", sender: nil)
            //This is where you should add the functionality of successful login
            //i.e. dismissing this view or push the home view controller etc
            }
        }
    }
}

class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?
    
    func startBackgroundMusic(backgroundMusicFileName: String) {
        if let bundle = Bundle.main.path(forResource: backgroundMusicFileName, ofType: "wav") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
    
    func playSoundEffect(soundEffect: String) {
        if let bundle = Bundle.main.path(forResource: soundEffect, ofType: "wav") {
            let soundEffectUrl = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:soundEffectUrl as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
}
