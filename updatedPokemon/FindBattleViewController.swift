//
//  FindBattleViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/11/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import CoreLocation


class FindBattleViewController: UIViewController,CLLocationManagerDelegate {
    

    let locationManager = CLLocationManager()
    var user: User!
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    let ref = Database.database().reference(withPath: "online-users")

    
    override func viewDidLoad() {
      super.viewDidLoad()
      self.locationManager.requestWhenInUseAuthorization()
      //self.locationManager.requestAlwaysAuthorization()

      // For use in foreground
      self.locationManager.requestWhenInUseAuthorization()
        


      if CLLocationManager.locationServicesEnabled() {
          locationManager.delegate = self
          locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
          locationManager.startUpdatingLocation()
      }
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
        //print("we get here!")
        if (curruser != nil){
            let badchar: Set<Character> = ["@", "."]
            var nocharemail = thecurremail
            nocharemail!.removeAll(where: { badchar.contains($0) })
            let newUserRef = self.ref.child(nocharemail!)
            self.ref.child(nocharemail!).runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
                if let post = currentData.value as? [String : AnyObject]{
                    let wins = post["wins"] as? Int ?? 0
                    newUserRef.updateChildValues(["wins": wins])
                    newUserRef.updateChildValues(["addedByUser": self.thecurremail!,"latval": locValue.latitude,"longval": locValue.longitude, "ingame": false])
                    return TransactionResult.success(withValue: currentData)
                }
                return TransactionResult.success(withValue: currentData)
            }){ (error, committed, snapshot) in
              if let error = error {
                print(error.localizedDescription)
              }
            }
        }
        //segue to Table.
        self.performSegue(withIdentifier: "showNearby", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.destination is FindBattleTableViewController{
            let vc = segue.destination as? FindBattleTableViewController
            locationManager.stopUpdatingLocation()
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
