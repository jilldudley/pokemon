//
//  GameViewController.swift
//  updatedPokemon
//
//  Created by Jill Dudley on 1/28/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import Speech
import Firebase
import FirebaseAuth
import CoreMotion

class GameViewControllerSlave: UIViewController, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var scoreLabel1:UILabel?
    @IBOutlet weak var scoreLabel2:UILabel?
    @IBOutlet weak var nameLabel1:UILabel?
    @IBOutlet weak var nameLabel2:UILabel?
    @IBOutlet weak var pokemon1:UIImageView?
    @IBOutlet weak var pokemon2:UIImageView?
    @IBOutlet weak var moveLabel1:UILabel?
    @IBOutlet weak var moveLabel2:UILabel?
    @IBOutlet weak var moveLabel3:UILabel?
    @IBOutlet weak var moveLabel4:UILabel?
    
    var battle:OnlineBattle?
    let battle_ref = Database.database().reference(withPath: "online-battles")
    let user_ref = Database.database().reference(withPath: "online-users")
    var user: User!
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    
    var timer:Timer?
    var seconds = 3599
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    //let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    var name = "squirftle"
    var name1 = "pikafchu"
    var moves1 = ["thunder", "strike", "nightmare", "shock"]
	var colors = [UIColor.white, UIColor.white, UIColor.white, UIColor.white]
    var power1 = [12.0,9.0,15.0,6.0]
	var types = [0,0,0,0]
    var flag1 = false
    var flag2 = false
    var attack = 15.0
    var op_attack = 7.0
    var op_defense = 10.0
    var defense = 8.0
    var tm = 1.0
    var speed = 10.0
    var op_speed = 5.0
    var active = false
    
    var id:String = "test1"
    
    // Player Health Variables
    var player1_score = 0.0
    var player2_score = 0.0
    var player1_health = 50.0;
    var player2_health = 50.0;
    
    var p1h = false;
    var pldam = 0;
    var other = false;
    
    var overr = 0
    
    var rst = false
    var lrc = -1
    var busy = false
    
    // Voice Recognition
    var request = SFSpeechAudioBufferRecognitionRequest()
    
    func recordAndRecognizeSpeech() {
        MusicPlayer.shared.stopBackgroundMusic()
        self.flag1 = false
        self.flag2 = false
        request = SFSpeechAudioBufferRecognitionRequest()
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 524288, format: recordingFormat) {buffer, _ in self.request.append(buffer)}
        audioEngine.prepare()
        do{
            try audioEngine.start()
        } catch {
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            return
        }
        if !myRecognizer.isAvailable{
            return
        }
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: {result, error in if let result = result{
            var bestString = result.bestTranscription.formattedString
            bestString=bestString.lowercased();
			if(!(self.battle?.p2c ?? false) && !self.busy){
                if (bestString.contains(self.name1) && bestString.contains(self.moves1[0])) {
                    self.flag1=true
					self.busy = true
                    self.isGameLive.toggle()
                    self.toggleCountdown(on: self.isGameLive, moven: 0, p: 1)
                    self.moveLabel1?.textColor = UIColor.green
                    self.moveLabel2?.textColor = UIColor.brown
                    self.moveLabel3?.textColor = UIColor.brown
                    self.moveLabel4?.textColor = UIColor.brown
                }
                else if(bestString.contains(self.name1) && bestString.contains(self.moves1[1])) {
                    self.flag1=true
					self.busy = true
                    self.isGameLive.toggle()
                    self.toggleCountdown(on: self.isGameLive, moven: 1, p: 1)
                    self.moveLabel2?.textColor = UIColor.green
                    self.moveLabel1?.textColor = UIColor.brown
                    self.moveLabel3?.textColor = UIColor.brown
                    self.moveLabel4?.textColor = UIColor.brown
                }
                else if (bestString.contains(self.name1) && bestString.contains(self.moves1[2])) {
                    self.flag1=true
					self.busy = true
                    self.isGameLive.toggle()
                    self.toggleCountdown(on: self.isGameLive, moven: 2, p: 1)
                    self.moveLabel3?.textColor = UIColor.green
                    self.moveLabel2?.textColor = UIColor.brown
                    self.moveLabel1?.textColor = UIColor.brown
                    self.moveLabel4?.textColor = UIColor.brown
                }
                else if (bestString.contains(self.name1) && bestString.contains(self.moves1[3])) {
                    self.flag1=true
					self.busy = true
                    self.isGameLive.toggle()
                    self.toggleCountdown(on: self.isGameLive, moven: 3, p: 1)
                    self.moveLabel4?.textColor = UIColor.green
                    self.moveLabel2?.textColor = UIColor.brown
                    self.moveLabel3?.textColor = UIColor.brown
                    self.moveLabel1?.textColor = UIColor.brown
                }
                if(self.flag1 || self.flag2){
                    self.updateScoreLabel1()
                    self.request.endAudio()
                    self.audioEngine.stop()
                    if self.audioEngine.inputNode.numberOfInputs > 0 {
                        self.audioEngine.inputNode.removeTap(onBus: 0)
                    }
                    self.recognitionTask?.cancel()
                    self.recognitionTask?.finish()
                    self.rst = true
                }
            }
        } else if let error = error{
            print(error)
            }
        })
        MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "Music Files/beginBattle")
    }
    
    // Motion Detection
    var gameTimer = Timer()
    var timeLeft = 11
    var isGameLive = false
    var failed = false
    var motionDetected = false
    var buttonClicked = false
    var motion = CMMotionManager()
    var xAccel = 0.0
    var yAccel = 0.0
    var zAccel = 0.0
    @IBOutlet weak var coachmark: UILabel!
    @IBOutlet weak var centerText: UILabel!
    @IBOutlet weak var coachmarkImage: UIImageView!
    
    @IBAction func startMotion(_ sender: Any) {
        isGameLive.toggle()
        toggleCountdown(on: isGameLive, moven: 0, p: 0)
    }
    
    var flagNightmare = false
    func detectMoveTwo() {
        if (flagNightmare == false) {
            coachmarkImage?.image=UIImage(named: "dir(-z)")
            if (self.zAccel < -1 && self.xAccel < 0.1) {
                coachmarkImage?.image=UIImage(named: "dir(-y)")
                flagNightmare = true
            }
        }
        if (flagNightmare == true && self.yAccel < -1) {
            
            flagNightmare = false
            motionDetected = true
            // nightmareDetected = true
        }
    }
    
    var flagShock = false
    func detectMoveThree() {
        if (flagShock == false) {
            coachmarkImage?.image=UIImage(named: "dir(-y)")
            if (self.yAccel < -1) {
                coachmarkImage?.image=UIImage(named: "dir(+y)")
                flagShock = true
            }
        }
        if (flagShock == true && self.yAccel > 1) {
            flagShock = false
            motionDetected = true
            // shockDetected = true
        }
    }
    
    var flagThunder = false
    func detectMoveZero() {
        if (flagThunder == false) {
            coachmarkImage?.image=UIImage(named: "dir(-y)")
            if (self.yAccel < -1) {
                coachmarkImage?.image=UIImage(named: "dir(-x)")
                flagThunder = true
            }
        }
        if (flagThunder == true && self.xAccel < -1) {
            flagThunder = false
            motionDetected = true
            // thunderDetected = true
        }
    }
    
    var flagStrike = false
    func detectMoveOne() {
        if (flagStrike == false) {
            coachmarkImage?.image=UIImage(named: "dir(-z)")
            if (self.zAccel < -1) {
                coachmarkImage?.image=UIImage(named: "dir(+x)")
                flagStrike = true
            }
        }
        if (flagStrike == true && self.xAccel > 1) {
            flagStrike = false
            motionDetected = true

            // strikeDetected = true
        }
    }
    
    func firstAction(continue: Bool, move: Int, pl: Int) {
        coachmark.text = "Shake the phone to execute the move!"
        coachmarkImage.alpha = 1
        if motionDetected {
            gameTimer.invalidate()
            centerText.text = "Good Hit!"
            self.battle?.p2d = Int(self.power1[move] * (self.op_attack/self.defense) * self.tm * Double.random(in: 0.8 ... 1.2))
			self.battle?.p2t = self.types[move]
            self.battle?.p2c = true
            self.battle_ref.child("\(self.id)/p2d").setValue(self.battle?.p2d)
            self.battle_ref.child("\(self.id)/p2t").setValue(self.battle?.p2t)
            self.battle_ref.child("\(self.id)/p2c").setValue(self.battle?.p2c)
            motionDetected = false
            motion.stopDeviceMotionUpdates()
            timeLeft = 11
            isGameLive.toggle()
            reportAttack(moveType: self.battle!.p2t, value: self.battle!.p2d)
            coachmarkImage.alpha = 0
        }
        else if timeLeft == 0 {
            gameTimer.invalidate()
            self.battle?.p2d = 0
            self.battle?.p2c = true
            self.battle_ref.child("\(self.id)/p2d").setValue(self.battle?.p2d)
            self.battle_ref.child("\(self.id)/p2c").setValue(self.battle?.p2c)
            centerText.text = "You Missed!"
            motion.stopDeviceMotionUpdates()
            timeLeft = 11
            isGameLive.toggle()
            coachmark.text = "You missed! Waiting for opponent!"
            coachmarkImage.alpha = 0
        }
    }
    func reportAttack(moveType: Int, value: Int) {
        if (moveType == 0) {
             coachmark.text = "You decreased your opponents health by " + String(value) + "!"
        }
        else if (moveType == 1) {
            coachmark.text = "You decreased your opponents defense!"
        }
        else if (moveType == 2) {
            coachmark.text = "You decreased your opponents attack!"
        }
        else {
            coachmark.text = "Unlucky hit, try again tomorrow."
        }
    }
    
    
    func toggleCountdown(on: Bool, moven: Int, p: Int) {
        gameTimer.invalidate()
        motion.deviceMotionUpdateInterval = 0.15
        motion.startDeviceMotionUpdates(to: OperationQueue.current!) {
            // Handler function
            (data, error) in print(data as Any)
            if let trueData = data {
                self.view.reloadInputViews()
                // Using global variable here
                self.xAccel = trueData.userAcceleration.x
                self.yAccel = trueData.userAcceleration.y
                self.zAccel = trueData.userAcceleration.z
				if(!self.motionDetected){
					if (moven == 0) {
						self.detectMoveZero()
					} else if (moven == 1) {
						self.detectMoveOne()
					} else if (moven == 2) {
						self.detectMoveTwo()
					} else if (moven == 3) {
						self.detectMoveThree()
					}
				}
            }
        }
        if on {
            // Access gameTimer object.
            gameTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
                guard let strongSelf = self else { return }
                strongSelf.timeLeft -= 1
                strongSelf.centerText.text = String(strongSelf.timeLeft)
                // Call function based on timer queue.
                // strongSelf.buttonClickAction(continue: true)
                strongSelf.firstAction(continue: true, move: moven, pl: p)
            })
        } else {
            gameTimer.invalidate()
        }
    }
    
    @IBOutlet weak var SlaveStatsP1: UILabel!
    @IBOutlet weak var SlaveStatsP2: UILabel!
    
    func updateScoreLabel1() -> Bool{
        if (player1_health <= 0){
            player1_health = 0
        }
        scoreLabel1?.text = String(format: "%.0f", player1_health)
        SlaveStatsP1?.text = "Def: " + String(format: "%.1f", battle!.d1) + " Att: " + String(format: "%.1f", battle!.a1)
        if(player1_health == 0){
            finishGame()
            return true
        }
        return false
    }
    
    func updateScoreLabel2()  -> Bool{
        if (player2_health <= 0){
            player2_health = 0
        }
        scoreLabel2?.text = String(format: "%.0f", player2_health)
        SlaveStatsP2?.text = "Def: " + String(format: "%.1f", battle!.d2) + " Att: " + String(format: "%.1f", battle!.a2)
        if(player2_health == 0){
            finishGame()
            return true
        }
        return false
    }
    
    func finishGame()
    {
        timer?.invalidate()
        timer = nil
        var alert:UIAlertController
        if(player1_health > 0){
            alert = UIAlertController(title: "Game Over!", message: "Your Pokemon Fainted!", preferredStyle: .alert)
            self.coachmark.text = "Game Over"
            UIView.animate(withDuration:1.0, animations: {
                self.pokemon2?.transform = CGAffineTransform(rotationAngle: CGFloat(280))
            })
        }
        else{
            alert = UIAlertController(title: "Game Over!", message: "You Won!", preferredStyle: .alert)
            self.coachmark.text = "Game Over"
            UIView.animate(withDuration:1.0, animations: {
                self.pokemon1?.transform = CGAffineTransform(rotationAngle: CGFloat(280))
            })
            let badchar: Set<Character> = ["@", "."]
            var nocharemail = thecurremail
            nocharemail!.removeAll(where: { badchar.contains($0) })
            user_ref.child(nocharemail!).runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
                if var post = currentData.value as? [String : AnyObject]{
                    var wins = post["wins"] as? Int ?? 0
                    wins += 1
                    self.user_ref.child(nocharemail!).updateChildValues(["wins": wins])
                    return TransactionResult.success(withValue: currentData)
                }
                return TransactionResult.success(withValue: currentData)
            }){ (error, committed, snapshot) in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
        }
        alert.addAction(UIAlertAction(title: "New Game", style: .default) { _ in
            self.performSegue(withIdentifier: "newgame", sender: nil)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func attackanimation(order: Int, fr: Bool, sr: Bool) {
        var x1 = CGFloat(300)
        var y1 = CGFloat(100)
        var x2 = CGFloat(300)
        var y2 = CGFloat(100)
        if(fr){
            x1 = CGFloat(500)
            y1 = CGFloat(0)
        }
        if(sr){
            x2 = CGFloat(500)
            y2 = CGFloat(0)
        }
        //two first
        if(order==2){
            UIView.animate(withDuration: 0.5, delay: 0.3, options: [ .curveEaseOut], animations: {
                self.pokemon2?.center.x -= x2
                self.pokemon2?.center.y -= y2
            }, completion: nil)
            UIView.animate(withDuration: 1.5, delay: 0.8, options: [ .curveEaseOut], animations: {
                self.pokemon2?.center.x += x2
                self.pokemon2?.center.y += y2
            }, completion: { (finished) in
                guard finished else {
                    return
                }
                if(self.updateScoreLabel1()){
                    return
                }
                UIView.animate(withDuration: 0.5, delay: 0.3, options: [ .curveEaseOut], animations: {
                    self.pokemon1?.center.x += x1
                    self.pokemon1?.center.y -= y1
                }, completion: nil)
                UIView.animate(withDuration: 1.5, delay: 0.8, options: [ .curveEaseOut], animations: {
                    self.pokemon1?.center.x -= x1
                    self.pokemon1?.center.y += y1
                }, completion: { (finished) in
                    guard finished else {
                        return
                    }
                    if(self.updateScoreLabel2()){
                        return
                    }
					self.moveLabel1?.textColor = self.colors[0]
                    self.moveLabel2?.textColor = self.colors[1]
                    self.moveLabel3?.textColor = self.colors[2]
                    self.moveLabel4?.textColor = self.colors[3]
                    self.reportOpponentsAttack(moveType: self.battle!.p1t, value: self.battle!.p1d)
                })
            })
        }
            //one first
        else{
            UIView.animate(withDuration: 0.5, delay: 0.3, options: [ .curveEaseOut], animations: {
                self.pokemon1?.center.x += x1
                self.pokemon1?.center.y -= y1
            }, completion: nil)
            UIView.animate(withDuration: 1.5, delay: 0.8, options: [ .curveEaseOut], animations: {
                self.pokemon1?.center.x -= x1
                self.pokemon1?.center.y += y1
            }, completion: { (finished) in
                guard finished else {
                    return
                }
                if(self.updateScoreLabel2()){
                    return
                }
                UIView.animate(withDuration: 0.5, delay: 0.3, options: [ .curveEaseOut], animations: {
                    self.pokemon2?.center.x -= x2
                    self.pokemon2?.center.y -= y2
                }, completion: nil)
                UIView.animate(withDuration: 1.5, delay: 0.8, options: [ .curveEaseOut], animations: {
                    self.pokemon2?.center.x += x2
                    self.pokemon2?.center.y += y2
                }, completion: { (finished) in
                    guard finished else {
                        return
                    }
                    if(self.updateScoreLabel1()){
                        return
                    }
					self.moveLabel1?.textColor = self.colors[0]
                    self.moveLabel2?.textColor = self.colors[1]
                    self.moveLabel3?.textColor = self.colors[2]
                    self.moveLabel4?.textColor = self.colors[3]
                   self.reportOpponentsAttack(moveType: self.battle!.p1t, value: self.battle!.p1d)
                })
            })
        }
    }
    
    func reportOpponentsAttack(moveType: Int, value: Int) {
           if (moveType == 0) {
                coachmark.text = "The opponent decreased your health by " + String(value) + "! Now say a new attack!"
           }
           else if (moveType == 1) {
               coachmark.text = "The opponent decreased your defense! Now say a new attack!"
           }
           else if (moveType == 2) {
               coachmark.text = "The opponent decreased your attack! Now say a new attack!"
           }
       }
       
    
    func execute(ord: Int)
    {
		if(self.battle?.p2t ?? 0 == 0){
			self.player1_health = self.player1_health - Double(self.battle?.p2d ?? 0)
		}
		else if(self.battle?.p2t ?? 0 == 1){
			self.defense = self.defense - Double(self.battle?.p2d ?? 0)/10
		}
		else{
			self.attack = self.attack - Double(self.battle?.p2d ?? 0)/10
		}
		if(self.battle?.p1t ?? 0 == 0){
			self.player2_health = self.player2_health - Double(self.battle?.p1d ?? 0)
		}
		else if(self.battle?.p1t ?? 0 == 1){
			self.op_defense = self.op_defense - Double(self.battle?.p1d ?? 0)/10
		}
		else{
			self.op_attack = self.op_attack - Double(self.battle?.p1d ?? 0)/10
		}
        if(self.battle?.p1d ?? 0 > 0){
            if(self.battle?.p2d ?? 0 > 0){
                attackanimation(order: ord, fr: true, sr: true)
            }
            else{
                attackanimation(order: ord, fr: true, sr: false)
            }
        }
        else{
            if(self.battle?.p2d ?? 0 > 0){
                attackanimation(order: ord, fr: false, sr: true)
            }
            else{
                attackanimation(order: ord, fr: false, sr: false)
            }
        }
    }
    
    func updatePlayer1(){
        self.name = battle?.playerInitPokemonName ?? "1"
        nameLabel1?.text = name
        pokemon1?.image=UIImage(named: name)
		if(name=="pikachu"  || name=="charizard"){
			pokemon1?.transform = CGAffineTransform(scaleX: -1,y: 1)
		}
        player1_health = battle?.playerInitPokemonHealth ?? 1
        speed = battle?.s1 ?? 0
        attack = battle?.a1 ?? 0
        defense = battle?.d1 ?? 0
        updateScoreLabel1()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        //update battle
        battle_ref.child("\(self.id)").observe(.value, with: { (snapshot) in
            self.battle = OnlineBattle(snapshot: snapshot)
            self.updatePlayer1()
            print(snapshot.value as Any)
        })
        self.battle_ref.child("\(self.id)/update").setValue("yes")
        self.name = battle?.playerInitPokemonName ?? "1"
        self.name1 = battle?.playerJoinPokemonName ?? "2"
        nameLabel1?.text = name
        nameLabel2?.text = name1
        updateScoreLabel1()
        updateScoreLabel2()
        //player2AttackForward()
        //player2AttackRetreat()
        if(name1=="pikachu"){
            moves1 = ["thunder", "strike", "nightmare", "shock"]
            power1 = [15.0,12.0,18.0,10.0]
			types = [0,0,1,0]
        }
        else if(name1=="squirtle"){
            moves1 = ["water", "drip", "punch", "strike"]
            power1 = [13.0,9.0,15.0,11.0]
			types = [0,2,0,0]
        }
        else{
            moves1 = ["fire", "spit", "fly", "punch"]
            power1 = [9.0,9.0,8.0,10.0]
			types = [0,2,1,0]
        }
		for n in 0...3{
			if(types[n] == 0){
				colors[n] = UIColor.black
			}
			else if(types[n] == 1){
				colors[n] = UIColor.blue
			}
			else{
				colors[n] = UIColor.red
			}
		}
        pokemon2?.image=UIImage(named: name1)
		if(name1=="squirtle"){
			pokemon2?.transform = CGAffineTransform(scaleX: -1,y: 1)
		}
        moveLabel1?.text = moves1[0]
        moveLabel2?.text = moves1[1]
        moveLabel3?.text = moves1[2]
        moveLabel4?.text = moves1[3]
		moveLabel1?.textColor = self.colors[0]
		moveLabel2?.textColor = self.colors[1]
		moveLabel3?.textColor = self.colors[2]
		moveLabel4?.textColor = self.colors[3]
        coachmark.numberOfLines = 3
        coachmark.text = name1 + " say a command!"
        centerText.text = " "
//        coachmarkImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / -2))
        coachmarkImage.alpha = 0
        
        
        
        recordAndRecognizeSpeech()
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                if self.seconds == 0{
                    self.finishGame()
                }
                else if self.seconds <= 3599 {
                    if(self.battle?.p1r ?? false && Int(self.battle?.roundcounter ?? 0) > self.lrc){
                        if(!self.active){
                            self.active = true
                            self.lrc = Int(self.battle?.roundcounter ?? 0)
                            self.execute(ord: self.battle?.fir ?? 0)
                            self.battle?.p2r = true
                            self.active = false
                            self.flag1 = false
                            //self.battle_ref.child("\(self.id)").setValue(self.battle?.toAnyObject())
                            self.battle_ref.child("\(self.id)/p2r").setValue(self.battle?.p2r)
                        }
                    }
                    if self.seconds % 20 == 0{
                        self.request.endAudio()
                        self.audioEngine.stop()
                        if self.audioEngine.inputNode.numberOfInputs > 0 {
                            self.audioEngine.inputNode.removeTap(onBus: 0)
                        }
                        self.recognitionTask?.cancel()
                        self.recognitionTask?.finish()
                        self.rst = true
                    }
                    self.seconds -= 1
                    //self.updateTimeLabel()
                    if(self.rst == true){
                        self.recordAndRecognizeSpeech()
                        self.rst = false
						self.busy = false
                    }
                }
            }
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func dismissKeyboard(){
        overr = 1
    }
}
