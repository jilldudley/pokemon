//
//  OnlineBattle.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/12/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import Firebase

struct OnlineBattle {
  
	let ref: DatabaseReference?
	let key: String
	let playerInit: String
	let playerJoin: String
	var playerInitPokemonName: String
	var playerJoinPokemonName: String
	var playerInitPokemonHealth: Double
	var playerJoinPokemonHealth: Double
 	var roundcounter: Double
	var p1c: Bool
	var p2c: Bool
	var fir: Int
	var p1d: Int
	var p2d: Int
	var p1r: Bool
	var p2r: Bool
	var p1t: Int
	var p2t: Int
	var a1: Double
	var a2: Double
	var d1: Double
	var d2: Double
	var s1: Double
	var s2: Double
	//create new struct
	init(playerInit: String, playerJoin: String, playerInitPokemonName: String, playerJoinPokemonName: String, playerInitPokemonHealth: Double, playerJoinPokemonHealth: Double, key: String = "", a1: Double, a2: Double, d1: Double, d2: Double, s1: Double, s2: Double) {
		self.ref = nil
		self.key = key
		self.playerInit = playerInit
		self.playerJoin = playerJoin
		self.playerInitPokemonName = playerInitPokemonName
		self.playerJoinPokemonName = playerJoinPokemonName
		self.playerInitPokemonHealth = playerInitPokemonHealth
		self.playerJoinPokemonHealth = playerJoinPokemonHealth
		self.a1 = a1
		self.a2 = a2
		self.d1 = d1
		self.d2 = d2
		self.s1 = s1
		self.s2 = s2
		self.roundcounter = 0
		self.p1c = false
		self.p2c = false
		self.fir = 0
		self.p1d = 0
		self.p2d = 0
		self.p1t = 0
		self.p2t = 0
		self.p1r = false
		self.p2r = false
  }
  //take from firebase
  init?(snapshot: DataSnapshot) {
    guard
		let value = snapshot.value as? [String: AnyObject],
		let playerInit = value["playerInit"] as? String,
		let playerJoin = value["playerJoin"] as? String,
		let playerInitPokemonName = value["playerInitPokemonName"] as? String,
		let playerJoinPokemonName = value["playerJoinPokemonName"] as? String,
		let playerInitPokemonHealth = value["playerInitPokemonHealth"] as? Double,
		let playerJoinPokemonHealth = value["playerJoinPokemonHealth"] as? Double,
		let roundcounter = value["roundcounter"] as? Double,
		let p1c = value["p1c"] as? Bool,
		let p2c = value["p2c"] as? Bool,
		let p1d = value["p1d"] as? Int,
		let p2d = value["p2d"] as? Int,
		let p1t = value["p1t"] as? Int,
		let p2t = value["p2t"] as? Int,
		let p1r = value["p1r"] as? Bool,
		let p2r = value["p2r"] as? Bool,
		let a1 = value["a1"] as? Double,
		let a2 = value["a2"] as? Double,
		let d1 = value["d1"] as? Double,
		let d2 = value["d2"] as? Double,
		let s1 = value["s1"] as? Double,
		let s2 = value["s2"] as? Double,
		let fir = value["fir"] as? Int
		else {
			return nil
		}
		self.ref = snapshot.ref
		self.key = snapshot.key
		self.playerInit = playerInit
		self.playerJoin = playerJoin
		self.playerInitPokemonName = playerInitPokemonName
		self.playerJoinPokemonName = playerJoinPokemonName
		self.playerInitPokemonHealth = playerInitPokemonHealth
		self.playerJoinPokemonHealth = playerJoinPokemonHealth
		self.roundcounter = roundcounter
		self.p1c = p1c
		self.p2c = p2c
		self.p1d = p1d
		self.p2d = p2d
		self.p1t = p1t
		self.p2t = p2t
		self.p1r = p1r
		self.p2r = p2r
		self.fir = fir
		self.a1 = a1
		self.a2 = a2
		self.d1 = d1
		self.d2 = d2
		self.s1 = s1
		self.s2 = s2
  }
  //updates firebase
  func toAnyObject() -> Any {
    return [
      "playerInit": playerInit,
      "playerJoin": playerJoin,
      "playerInitPokemonName": playerInitPokemonName,
      "playerJoinPokemonName": playerJoinPokemonName,
      "playerInitPokemonHealth":playerInitPokemonHealth,
      "playerJoinPokemonHealth": playerJoinPokemonHealth,
      "roundcounter": roundcounter,
	  "p1c": p1c,
	  "p2c": p2c,
	  "fir": fir,
	  "p1r": p1r,
	  "p2r": p2r,
	  "p1d": p1d,
	  "p1t": p1t,
	  "p2t": p2t,
	  "a1": a1,
	  "a2": a2,
	  "d1": d1,
	  "d2": d2,
	  "s1": s1,
	  "s2": s2,
	  "p2d": p2d
    ]
  }
}
