//
//  FindBattleViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/11/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth


class PRViewController: UIViewController {
    
    @IBOutlet weak var pokemon1:UIImageView?

    var user: User!
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    let ref = Database.database().reference(withPath: "online-users")
	var ptype:String = "pikachu"

    
    override func viewDidLoad() {
		super.viewDidLoad()
		let badchar: Set<Character> = ["@", "."]
		var nocharemail = thecurremail
		nocharemail!.removeAll(where: { badchar.contains($0) })
		ref.child("\(nocharemail!)/char").setValue(self.ptype)
		pokemon1?.image=UIImage(named: self.ptype)
    }

    

    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
