//
//  FindBattleViewController.swift
//  updatedPokemon
//
//  Created by Alexis Gehrke on 2/11/20.
//  Copyright © 2020 Jill Dudley. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import CoreLocation


class FindBattleTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    var Users: [OnlineUser] = []
    let user_ref = Database.database().reference(withPath: "online-users")
    let battle_ref = Database.database().reference(withPath: "online-battles")
    let curruser = Auth.auth().currentUser;
    let thecurremail = Auth.auth().currentUser?.email;
    var userLat: Double = 0.0
    var userLong: Double = 0.0
    var battle:OnlineBattle?
    var ptype:String = "pikachu"
    var attack:Double = 10
    var defense:Double = 10
    var speed:Double = 10
    var health:Int = 50
    var id:String = "test1"
    
    override func viewDidLoad() {
      super.viewDidLoad()
		let badchar: Set<Character> = ["@", "."]
		var nocharemail = thecurremail
		nocharemail!.removeAll(where: { badchar.contains($0) })
		user_ref.child(nocharemail!).runTransactionBlock({ (currentData1: MutableData) -> TransactionResult in
			if let post = currentData1.value as? [String : AnyObject]{
				self.attack = post["attack"] as? Double ?? 0
				self.defense = post["defense"] as? Double ?? 0
				self.speed = post["speed"] as? Double ?? 0
				self.health = post["health"] as? Int ?? 0
				self.ptype = post["char"] as? String ?? "pikachu"
				self.userLat = post["latval"] as? Double ?? 0
				self.userLong = post["longval"] as? Double ?? 0
				return TransactionResult.success(withValue: currentData1)
			}
			return TransactionResult.success(withValue: currentData1)
		}){ (error, committed, snapshot) in
			if let error = error {
				print(error.localizedDescription)
			}
		}

        
        user_ref.queryOrdered(byChild: "latval").observe(.value, with: { snapshot in
            var newUsers: [OnlineUser] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let onlineUsers = OnlineUser(snapshot: snapshot) {
                    if (onlineUsers.addedByUser == self.curruser?.email){
                        self.userLat = onlineUsers.latval
                        self.userLong = onlineUsers.longval
                        //if (onlineUsers.ingame == true){
                            //self.performSegue(withIdentifier: "beginBattle", sender: nil)
                        //}
                    }
                }
            }
            for child in snapshot.children {
              if let snapshot = child as? DataSnapshot,
                let onlineUsers = OnlineUser(snapshot: snapshot) {
                if (onlineUsers.addedByUser != self.curruser?.email){
                    print(self.userLat, self.userLong)
                    if ((abs(onlineUsers.latval - self.userLat) < 0.001) && (abs(onlineUsers.longval - self.userLong) < 0.001)){
                        newUsers.append(onlineUsers)
                    }
                }
              }
            }
            self.Users = newUsers
            print(newUsers)
            self.tableView.reloadData()
        })
    }

    // MARK: UITableView Delegate methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return Users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
      let onlineUser = Users[indexPath.row]
        cell.textLabel?.text = onlineUser.addedByUser
      cell.detailTextLabel?.text = "nearby!"
      return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
      return true
    }
    
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//      if editingStyle == .delete {
//        let groceryItem = Users[indexPath.row]
//        groceryItem.ref?.removeValue()
//      }
 //   }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let onlineUser = Users[indexPath.row]
        let alert = UIAlertController(title: "Start a battle?",
                                      message: "Would you like to start a battle with " + onlineUser.addedByUser + "?",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Create", style: .default) { _ in
            self.battle = OnlineBattle(playerInit: self.thecurremail!, playerJoin: onlineUser.addedByUser, playerInitPokemonName: self.ptype, playerJoinPokemonName: "Waiting", playerInitPokemonHealth: Double(self.health), playerJoinPokemonHealth: 1111, key: "", a1: Double(self.attack), a2: 0, d1: Double(self.defense), d2:0, s1: Double(self.speed), s2: 0)

            let badchar: Set<Character> = ["@", "."]
            self.id = self.thecurremail! + " VS " + onlineUser.addedByUser
            self.id.removeAll(where: { badchar.contains($0) })
            let newUserRef = self.battle_ref.child("\(self.id)")
            newUserRef.setValue(self.battle?.toAnyObject())
            var goodInitEmail = self.thecurremail!
            goodInitEmail.removeAll(where: { badchar.contains($0) })
            self.user_ref.child(goodInitEmail).updateChildValues(["ingame": true])
            self.user_ref.removeAllObservers()
            self.performSegue(withIdentifier: "beginBattle", sender: nil)
        })
        alert.addAction(UIAlertAction(title: "Join", style: .default) { _ in
            self.battle = OnlineBattle(playerInit: self.thecurremail!, playerJoin: onlineUser.addedByUser, playerInitPokemonName: "Waiting", playerJoinPokemonName: self.ptype, playerInitPokemonHealth: 1111, playerJoinPokemonHealth: Double(self.health), key: "", a1: 0, a2: Double(self.attack), d1:0, d2: Double(self.defense), s1: 0, s2: Double(self.speed))
            let badchar: Set<Character> = ["@", "."]
            self.id = onlineUser.addedByUser + " VS " + self.thecurremail!
            self.id.removeAll(where: { badchar.contains($0) })
            var newUserRef = self.battle_ref.child("\(self.id)/playerJoinPokemonName")
            newUserRef.setValue(self.battle?.playerJoinPokemonName)
            newUserRef = self.battle_ref.child("\(self.id)/playerJoinPokemonHealth")
            newUserRef.setValue(self.battle?.playerJoinPokemonHealth)
            newUserRef = self.battle_ref.child("\(self.id)/a2")
            newUserRef.setValue(self.battle?.a2)
            newUserRef = self.battle_ref.child("\(self.id)/d2")
            newUserRef.setValue(self.battle?.d2)
            newUserRef = self.battle_ref.child("\(self.id)/s2")
            newUserRef.setValue(self.battle?.s2)
            var goodJoinEmail = onlineUser.addedByUser
            goodJoinEmail.removeAll(where: { badchar.contains($0) })
            self.user_ref.child(goodJoinEmail).updateChildValues(["ingame": true])
            self.user_ref.removeAllObservers()
            self.performSegue(withIdentifier: "beginBattle1", sender: nil)
        })
        alert.addAction(UIAlertAction(title: "No", style: .default))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.destination is GameViewController{
            let vc = segue.destination as? GameViewController
            vc?.battle=self.battle
            vc?.attack=Double(self.attack)
            vc?.defense=Double(self.defense)
            vc?.speed=Double(self.speed)
            vc?.player1_health=Double(self.health)
            vc?.id=self.id
        }
        else if segue.destination is GameViewControllerSlave{
            let vc = segue.destination as? GameViewControllerSlave
            vc?.battle=self.battle
            vc?.op_attack=Double(self.attack)
            vc?.op_defense=Double(self.defense)
            vc?.op_speed=Double(self.speed)
            vc?.player2_health=Double(self.health)
            vc?.id=self.id
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
